"use strict";
const BootBot = require("./BootBot");
const normalizeString = require("./utils/normalize-string");
const db = require('./db');
require("dotenv").config();
// const { Client } = require('pg');

// const client = new Client({
//   connectionString: process.env.DATABASE_URL,
//   ssl: true,
// });









const bot = new BootBot({
  accessToken: process.env.ACCESS_TOKEN,
  verifyToken: process.env.VERIFY_TOKEN,
  appSecret: process.env.APP_SECRET
});



bot.setGreetingText(
  "Aplikasi Zaqat.in adalah aplikasi yang memudahkanmu berzakat dengan sistem yang transparan, trackable dan reminder. \n\nZaqat.in akan menyalurkan zakatmu untuk modal usaha bagi orang yang kurang mampu 💙"
);
bot.setGetStartedButton("MULAI");

bot.setPersistentMenu([
  {
    "locale": "default",
    "composer_input_disabled": true,
    "call_to_actions": [
        {
            "type": "postback",
            "title": "📒 Menu",
            "payload": "MENU"
        }
    ]
}
]);
bot.on("postback:MENU", (payload, chat) => {
  chat.say({
    text: 'Pilih Menu',
    quickReplies: [
      {
        "content_type":"text",
        "title":" Atur Zakat  ",
        "payload":"ATUR_ZAKAT",
        "image_url":"https://img.icons8.com/color/64/000000/calendar-11.png"
      },
      {
        "content_type":"text",
        "title":" Riwayat Zakat  ",
        "payload":"RIWAYAT",
        "image_url":"https://img.icons8.com/color/48/000000/activity-history.png"
        
      },
      {
        "content_type":"text",
        "title":" Pengetahuan Zakat  ",
        "payload":"INFO",
        "image_url":"https://img.icons8.com/ultraviolet/40/000000/book-shelf.png"
        
      },
      {
        "content_type":"text",
        "title":" Laporan Ibadah  ",
        "payload":"PERFORM",
        "image_url":"https://img.icons8.com/ultraviolet/40/000000/ratings.png"
        
      },
    ]
  });
});

bot.on("quick_reply:RIWAYAT", (payload, chat) => {
  chat.sendTemplate(
    {
      template_type: "button",
      text: "Lihat Riwayat",
      buttons: [
        {
          type: "web_url",
          url:"https://zaqat.herokuapp.com/riwayat",
          title: "Klik Disini",
          webview_height_ratio: "tall",
          messenger_extensions: true
        }
      ]
    },

    { typing: true }
  )
})
bot.on("quick_reply:PERFORM", (payload, chat) => {
  chat.sendTemplate(
    {
      template_type: "button",
      text: "Lihat Ibadah Report Saya",
      buttons: [
        {
          type: "web_url",
          url:"https://zaqat.herokuapp.com/ibadahPerformance",
          title: "Klik Disini",
          webview_height_ratio: "compact",
          messenger_extensions: true
        }
      ]
    },

    { typing: true }
  )
})

bot.on("quick_reply:INFO", (payload, chat) => {
  chat.sendGenericTemplate([
    {
      "title":"Zakat Fitrah ?",
      "image_url":"https://cdn-x.sindonews.net/dyn/620/content/2019/05/27/69/1407906/inilah-syarat-wajib-menunaikan-zakat-fitrah-VqA.jpg",
      "subtitle":"Zakat fitrah adalah zakat yang wajib dikeluarkan umat Muslim menjelang hari raya Idul Fitri atau pada bulan Ramadan. Zakat fitrah dapat dibayar dengan setara 3,5 liter (2,5 kilogram) makanan pokok dari daerah yang bersangkutan. ",
      "default_action": {
        "type": "web_url",
            "url": "https://zaqat.herokuapp.com/aturzakat.html",
            "webview_height_ratio": "tall",
            "messenger_extensions": true
      },
      "buttons":[
        {
          "type": "web_url",
            "title": "Atur Zakat Sekarang",
            "url": "https://zaqat.herokuapp.com/aturzakat.html",
            "webview_height_ratio": "tall",
             "messenger_extensions": true
        }            
      ]      
    },
    {
      "title":"Zakat Maal ?",
      "image_url":"https://www.blibli.com/friends/assets/2019/05/emas600px1-1.jpg",
      "subtitle":"Zakat Maal adalah zakat harta yang wajib dikeluarkan seorang muslim sesuai dengan nishab dan haulnya. Waktu pengeluaran zakat jenis ini tidak dibatasi jadi bisa dikeluarkan sepanjang tahun ketika syarat zakat terpenuhi",
      "default_action": {
            "type": "web_url",
             "url": "https://zaqat.herokuapp.com/aturzakat.html",
            "webview_height_ratio": "tall",
            "messenger_extensions": true
      },
      "buttons":[
        {
          "type": "web_url",
            "title": "Atur Zakat Sekarang",
            "url": "https://zaqat.herokuapp.com/aturzakat.html",
            "webview_height_ratio": "tall",
            "messenger_extensions": true
        }            
      ]      
    },
    {
      "title":"Zakat Penghasilan?",
      "image_url":"https://sharinghappiness.org/files/programgalleries/ee316e8258ae5281f346a643de561212-20190529040801_thumbnail_x480.jpg",
      "subtitle":"Zakat penghasilan merupakan zakat yang perlu dikeluarkan setiap kita mendapatkan penghasilan yang berupa harta atau uang. Sama dengan zakat mal yang memiliki jangka waktu satu tahun, namun zakat penghasilan juga bisa dikeluarkan perbulan dengan cara dicicil dan dengan perhitungan yang berbeda.",
      "default_action": {
        "type": "web_url",
            "url": "https://zaqat.herokuapp.com/aturzakat.html",
            "webview_height_ratio": "tall",
            "messenger_extensions": true
      },
      "buttons":[
        {
          "type": "web_url",
            "title": "Atur Zakat Sekarang",
            "url": "https://zaqat.herokuapp.com/aturzakat.html",
            "webview_height_ratio": "tall",
            "messenger_extensions": true
        }            
      ]      
    }
  ], {typing: true})
})
bot.on("postback:INFO_PAYLOAD", (payload, chat) => {
  chat.sendGenericTemplate([
    {
      "title":"Zakat Fitrah ?",
      "image_url":"https://cdn-x.sindonews.net/dyn/620/content/2019/05/27/69/1407906/inilah-syarat-wajib-menunaikan-zakat-fitrah-VqA.jpg",
      "subtitle":"Zakat fitrah adalah zakat yang wajib dikeluarkan umat Muslim menjelang hari raya Idul Fitri atau pada bulan Ramadan. Zakat fitrah dapat dibayar dengan setara 3,5 liter (2,5 kilogram) makanan pokok dari daerah yang bersangkutan. ",
      "default_action": {
        "type": "web_url",
            "url": "https://zaqat.herokuapp.com/aturzakat.html",
            "webview_height_ratio": "tall",
            "messenger_extensions": true
      },
      "buttons":[
        {
          "type": "web_url",
            "title": "Atur Zakat Sekarang",
            "url": "https://zaqat.herokuapp.com/aturzakat.html",
            "webview_height_ratio": "tall",
             "messenger_extensions": true
        }            
      ]      
    },
    {
      "title":"Zakat Maal ?",
      "image_url":"https://www.blibli.com/friends/assets/2019/05/emas600px1-1.jpg",
      "subtitle":"Zakat Maal adalah zakat harta yang wajib dikeluarkan seorang muslim sesuai dengan nishab dan haulnya. Waktu pengeluaran zakat jenis ini tidak dibatasi jadi bisa dikeluarkan sepanjang tahun ketika syarat zakat terpenuhi",
      "default_action": {
            "type": "web_url",
             "url": "https://zaqat.herokuapp.com/aturzakat.html",
            "webview_height_ratio": "tall",
            "messenger_extensions": true
      },
      "buttons":[
        {
          "type": "web_url",
            "title": "Atur Zakat Sekarang",
            "url": "https://zaqat.herokuapp.com/aturzakat.html",
            "webview_height_ratio": "tall",
            "messenger_extensions": true
        }            
      ]      
    },
    {
      "title":"Zakat Penghasilan?",
      "image_url":"https://sharinghappiness.org/files/programgalleries/ee316e8258ae5281f346a643de561212-20190529040801_thumbnail_x480.jpg",
      "subtitle":"Zakat penghasilan merupakan zakat yang perlu dikeluarkan setiap kita mendapatkan penghasilan yang berupa harta atau uang. Sama dengan zakat mal yang memiliki jangka waktu satu tahun, namun zakat penghasilan juga bisa dikeluarkan perbulan dengan cara dicicil dan dengan perhitungan yang berbeda.",
      "default_action": {
        "type": "web_url",
            "url": "https://zaqat.herokuapp.com/aturzakat.html",
            "webview_height_ratio": "tall",
            "messenger_extensions": true
      },
      "buttons":[
        {
          "type": "web_url",
            "title": "Atur Zakat Sekarang",
            "url": "https://zaqat.herokuapp.com/aturzakat.html",
            "webview_height_ratio": "tall",
            "messenger_extensions": true
        }            
      ]      
    }
  ], {typing: true})
})

//when user subscribe (clicked get started button)
bot.on("postback:MULAI", (payload, chat) => {
  chat.getUserProfile().then(user => {
    console.log(user);
    db.query(`INSERT INTO account (user_id, nama, created_on) VALUES ('${user.id}','${user.first_name+" "+user.last_name}',current_date) ON CONFLICT (user_id) DO UPDATE SET last_login = current_date `)
    .then(res => console.log('berhasil ditambahkan'+res))
    .catch(err => console.log(err))
    
    
    chat.say(`Hi ${user.first_name}!`).then(() => {
      chat.say("yuk lihat cara kerja Zaqat").then(() => {
        chat
          .say(
            {
              attachment: "image",
              url: "https://zaqatin.firebaseapp.com/img/caraKerja.png"
            },
            { typing: true }
          )
          .then(() => {
            chat
              .sendTemplate(
                {
                  template_type: "button",
                  text: "lihat juga bukti nyatanya",
                  buttons: [
                    {
                      type: "web_url",
                      url:"https://zaqatin.firebaseapp.com/vid/zakatUntukUsaha.mp4",
                      title: "video",
                      webview_height_ratio: "compact"
                    }
                  ]
                },

                { typing: true }
              ).then(() => {
                chat
                  .sendTemplate(
                    {
                      template_type: "button",
                      text: "Yuk Bantu Mereka dengan Atur Zakatmu Sekarang !",
                      buttons: [
                        {
                          type: "web_url",
                          url:"https://zaqat.herokuapp.com/aturZakat",
                          title: "😃 Ya Saya Mau!",
                          webview_height_ratio: "tall",
                          messenger_extensions: true
                        },
                        {
                          type: "postback",
                          title: "☹️ Saya Tidak Peduli",
                          payload: "tidak"
                        }
                      ]
                    },
    
                    { typing: true }
                  )
                   
              });
              
          })
      });
    });
  });
});
bot.on("postback:BOOTBOT_BUTTON_SAYATIDAKPEDULI", (payload, chat) => {
  chat.say("Yahhh :(");
});

bot.start(process.env.PORT || 3000);
