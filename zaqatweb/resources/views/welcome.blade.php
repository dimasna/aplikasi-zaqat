@php
use App\Muzakki;
    $user = Muzakki::first();
@endphp

<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    {{-- <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet"> --}}

    <!-- Styles -->
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">
</head>

<body>
  <div class="loader">
    <div class="page-loader"></div>
  </div>

  <!-- Floating Shards -->
  <img src="img/1.png" alt="Shard" class="shard">

  <!-- Welcome Section -->
  <div class="welcome d-flex justify-content-center flex-column">
    <div class="inner-wrapper mt-auto mb-auto">
      <h1 class="slide-in">Zaqat Dashboard</h1>
      <p class="slide-in">Dashboard untuk analitik dan laporan zaqat.</p>
      <div class="action-links slide-in">
        <a href="{{ route('register') }}" class="btn btn-primary btn-pill align-self-center mr-2">
          <i class="fa fa-download mr-1"></i> Register</a>
        <a href="{{ route('login') }}" data-scroll-to="#introduction" id="scroll-to-content" class="btn btn-outline-light btn-pill align-self-center" style="color: #3490dc; border-color: #3490dc">Login</a>
      </div>
    </div>
    <div class="product-by">
      <a href="https://github.com/dimasna" target="_blank">
        <p>A Product By</p>
        <img src="img/icon.png" width="60" alt="Dimas N AL">
      </a>
    </div>
  </div>

  

  <!-- JavaScript -->
 
  <script async defer src="https://buttons.github.io/buttons.js"></script>
  <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
  
<!-- Scripts -->
<script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
        crossorigin="anonymous"></script>
<script src="{{ mix('js/app.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4"
        crossorigin="anonymous"></script>
</body>

</html>
